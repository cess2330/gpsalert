//
//  ViewController.swift
//  GPSAlert
//
//  Created by Cesar Castillo on 30/10/19.
//  Copyright © 2019 Cesar Castillo. All rights reserved.
//

import UIKit
import AVFoundation
import FirebaseMessaging
import MapKit


class ViewController: UIViewController {

    @IBOutlet weak var phoneNumberTextfield: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var containerMapView: UIView!
    @IBOutlet weak var containerStackView: UIStackView!
    
    @IBOutlet weak var mapViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var centerStacViewConstraint: NSLayoutConstraint!
    let locationManager = CLLocationManager()
    var audioPlayer: AVAudioPlayer!
    var registeredNumber = ""
    var userType = ""
    var originalMapRegion = MKCoordinateRegion()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self
        mapView.showsCompass = true
        mapView.isPitchEnabled = true
        mapView.isScrollEnabled = true
        mapView.isRotateEnabled = true
        mapView.isZoomEnabled = true
        phoneNumberTextfield.delegate = self
        validateUserType()
        
        originalMapRegion = mapView.region
        
        print(mapView.region)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateRemoteLocation), name:NSNotification.Name(rawValue: "updateLocation"), object: nil)
        
    }
    
    
    deinit {
       NotificationCenter.default.removeObserver(self,name: NSNotification.Name(rawValue: "updateLocation"),object: nil)
    }
    

    func validateUserType() {
        
        if UserDefaults.standard.value(forKey: "optionValue") as! String == "phoneA" {
            mapView.showsUserLocation = true
            userType = "A"
            titleLabel.text = "Introduce el número al que deseas enviar tu ubicación"
            registerButton.tag = 0
            locationManager.delegate = self
            locationManager.distanceFilter = 1
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.pausesLocationUpdatesAutomatically = false
            
        } else {
            userType = "B"
            titleLabel.text = "Introduce tu número teléfonico"
            registerButton.tag = 0
            
        }
        
    }
    
    @objc func updateRemoteLocation(notification: NSNotification) {
        if mapView.annotations.count > 0 {
            let allAnnotations = self.mapView.annotations
            self.mapView.removeAnnotations(allAnnotations)
        }
    
        let dict = (notification.userInfo)!
        let latitude = dict["latitud"] as! String
        let longitude = dict["longitud"] as! String
        let userRemoteLocation = MKPointAnnotation()
        userRemoteLocation.title = "Ubicación del usuario"
        userRemoteLocation.coordinate = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
        let span = MKCoordinateSpan(latitudeDelta: 0.0010, longitudeDelta: 0.0010)
        let region = MKCoordinateRegion(center: userRemoteLocation.coordinate, span: span)
        mapView.setRegion(region, animated: true)
        mapView.addAnnotation(userRemoteLocation)
        
    }
    
    
    func cleanMapView() {
        if mapView.annotations.count > 0 {
            let allAnnotations = self.mapView.annotations
            self.mapView.removeAnnotations(allAnnotations)
        }

        mapView.setRegion(originalMapRegion, animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        phoneNumberTextfield.resignFirstResponder()
    }
    
    
    func playSound() {
        if let soundURL = Bundle.main.url(forResource: "alertSound", withExtension: "mp3") {
            do {
                
                audioPlayer = try AVAudioPlayer(contentsOf: soundURL)
            }
            catch {
                print(error)
            }
            
            audioPlayer.play()
        }else{
            print("Unable to locate audio file")
        }
    }
    
    func animateView(toUp:Bool) {
        self.cleanMapView()
        if toUp {
             self.containerMapView.alpha = 1
            self.mapViewTopConstraint.constant = -85
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
                self.containerStackView.transform = CGAffineTransform(translationX: 0, y: -100)
                self.containerMapView.isHidden = false
                UIView.animate(withDuration: 0.5, animations: {
                    self.view.layoutIfNeeded()
                })
            }, completion: nil)
      
        } else {
            self.mapViewTopConstraint.constant = 15
            self.containerMapView.alpha = 1
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
                self.containerStackView.transform = .identity
                
                UIView.animate(withDuration: 0.5, animations: {
                    self.view.layoutIfNeeded()
                    self.containerMapView.alpha = 0.0
                    
                })
            }, completion: { _ in
                self.containerMapView.isHidden = true
            })
        }
        
    }
    

    @IBAction func registrarPressed(_ sender: UIButton) {
        switch sender.tag {
        case 0:
           
            if let phoneNumber = phoneNumberTextfield.text {
                if phoneNumber.count == 10 {
                    registerButton.tag = 1
                    phoneNumberTextfield.resignFirstResponder()
                    phoneNumberTextfield.isUserInteractionEnabled = false
                    registeredNumber = phoneNumber
                    UserDefaults.standard.set(phoneNumber, forKey: "phoneNumber")
                    if userType == "A" {
                        locationManager.startUpdatingLocation()
                        titleLabel.text = "Número con el que se comparte la ubicación"
                        registerButton.setTitle("Detener envio de ubicación", for: .normal)
                        animateView(toUp: true)
                        
                    } else {
                        Messaging.messaging().subscribe(toTopic: phoneNumber)
                        titleLabel.text = "Número propio"
                        registerButton.setTitle("Dejar de recibir ubicación", for: .normal)
                        animateView(toUp: true)
                        
                    }
                } else {
                    let alert = UIAlertController(title: "Logitud de número telefónico inválido", message: "El número telefónico debe contener un mínimo de 10 dígitos", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                    alert.view.tintColor = .init(red: 0.0/255.0, green: 145.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
            
        case 1:
            Messaging.messaging().unsubscribe(fromTopic: self.registeredNumber)
            UserDefaults.standard.removeObject(forKey: "phoneNumber")
            registerButton.setTitle("Registrar", for: .normal)
            registeredNumber = ""
            phoneNumberTextfield.isUserInteractionEnabled = true
            phoneNumberTextfield.text = ""
            registerButton.tag = 0
            locationManager.stopUpdatingLocation()
            if userType == "A" {
                animateView(toUp: false)
                titleLabel.text = "Introduce el número al que deseas enviar tu ubicación"
            } else {
                animateView(toUp: false)
                titleLabel.text = "Introduce tu número teléfonico"
                
            }
        default:
            break
        }
        
    }

}

extension ViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 10

        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}

extension ViewController : MKMapViewDelegate, CLLocationManagerDelegate {
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            let span = MKCoordinateSpan(latitudeDelta: 0.0010, longitudeDelta: 0.0010)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            mapView.setRegion(region, animated: true)
            let call = ApiRest()
            if self.registeredNumber != "" {
                playSound()
                call.sendNotification(tittle: "El usuario se ha movido", body: "El usuario se ha movido a estas coordenadas latitud: \(String(location.coordinate.latitude)), longitud: \(String(location.coordinate.longitude))", topic: Int(self.registeredNumber)!, latitud: String(location.coordinate.latitude), longitud: String(location.coordinate.longitude))
            }
            
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: (error)")
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        return nil
        
        
    }

}









