

//
//  ApiRest.swift
//  GPSAlert
//
//  Created by Cesar Castillo on 30/10/19.
//  Copyright © 2019 Cesar Castillo. All rights reserved.
//

import Foundation
import Alamofire

 protocol ApiRestDelegate {
    func connectionFinish(withError: String)
    func connectionFinishSuccessFirebase(withResponseIdDocument: String?,tipo : String)
    
}

class ApiRest {
    
    static let shared = ApiRest()
    
    var delegate: ApiRestDelegate?
    
      let apiKeyFirebase = "AAAAcjosvOk:APA91bGLOEwks16t72cBYTaN29giRr7PZU8c9zNrJXTbkuhsNXcd6eyqScqCfW1noIsRPLLVp7Fwimt_TvXmLUP-N-xeMY-g2GQMrVNkp8SxvViRnnXThO7b4OHkFaWRY23Fsc9V2Kai"
    
    
    
    
    func sendNotification(tittle : String,body :String, topic : Int,latitud : String, longitud : String) {
        var json = [String:Any]()
  
        
        let notificacion = ["body": body, "content_available": true,"priority": "high","title":tittle, "sound" : "default"] as [String : Any]
        
        let data = ["body": body, "content_available": true,"priority": "high","title":tittle,"sound" : "default","latitud" : latitud,"longitud" : longitud] as [String : Any]
        
        json = ["to" : "/topics/\(topic)",
            "notification": notificacion,
            "data": data]
        
        let jsonSting = json.dict2json()
        
        
        let urlString = "https://fcm.googleapis.com/fcm/send"
        
        let url = URL(string: urlString)!
        let jsonData = jsonSting.data(using: .utf8, allowLossyConversion: false)!
        
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=\(apiKeyFirebase)", forHTTPHeaderField: "Authorization")
        request.httpBody = jsonData
        
        
        Alamofire.request(request).responseJSON{ response in
            print(response)
            switch response.result {
            case .failure(let error):
                print(error)
                self.delegate?.connectionFinish(withError: "Un error ha ocurrido")
                return
                
            case .success(let data):
                guard let json = data as? [String : AnyObject] else {
                    print("Failed to get expected response from webserver.")
                    return
                }
                
                print(json)
                
            }
        }
    }
    
}

extension Dictionary {
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    
    func dict2json() -> String {
        return json
    }
}
