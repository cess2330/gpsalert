//
//  HomeViewController.swift
//  GPSAlert
//
//  Created by Cesar Castillo on 30/10/19.
//  Copyright © 2019 Cesar Castillo. All rights reserved.
//

import UIKit
import Firebase

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let phoneNumber = UserDefaults.standard.value(forKey: "phoneNumber") as? String {
            Messaging.messaging().unsubscribe(fromTopic: phoneNumber)
            UserDefaults.standard.removeObject(forKey: "phoneNumber")
        }

    }
    

    @IBAction func optionPressed(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            
            UserDefaults.standard.set("phoneA", forKey: "optionValue")
            
        case 1:
             UserDefaults.standard.set("phoneB", forKey: "optionValue")
        default:
            break
        }
        
        self.performSegue(withIdentifier: "DETAIL_SEGUE", sender: self)
        
    }
    
}
